<?php

return [
        "images"=>[
            'logo' => file_get_contents(public_path('/assets/img/condominio-verde-serrano/media/logo.svg')),
            'background-form' => '/assets/img/condominio-verde-serrano/media/MDR_FITNESS CENTER_LD.jpg',
            'background-description' => '/assets/img/condominio-verde-serrano/media/img-lp.jpg',
            'image-structure' => '/assets/img/condominio-verde-serrano/media/380e530098fe700f7a79ff822985940d.jpg',
            'home-plan-1' => '/assets/img/condominio-verde-serrano/media/MDR_APTO TIPO A PAV TER.jpg',
            'home-plan-2' => '/assets/img/condominio-verde-serrano/media/MDR_APTO TIPO D PAV TER.jpg',
            'home-plan-3' => '/assets/img/condominio-verde-serrano/media/MDR_APTO TIPO D PAV SS1.jpg',
            'tourism' => '/assets/img/condominio-verde-serrano/media/img-ipe-localizacao.jpg'
        ],
        "icons"=>[
            'arrow-right' => file_get_contents(public_path('/assets/img/condominio-verde-serrano/icons/arrow-right.svg')),
            'favicon' => '/assets/img/condominio-verde-serrano/icons/favicon.png',
            'facebook' => file_get_contents(public_path('/assets/img/condominio-verde-serrano/icons/facebook.svg')),
            'instagram' => file_get_contents(public_path('/assets/img/condominio-verde-serrano/icons/instagram.svg')),
            'whatsapp' => file_get_contents(public_path('/assets/img/condominio-verde-serrano/icons/whatsapp.svg')),
            'agency' => file_get_contents(public_path('/assets/img/condominio-verde-serrano/icons/NoPath.svg'))
            ]
];
