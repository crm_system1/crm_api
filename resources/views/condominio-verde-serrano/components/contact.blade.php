<div class=" w-100 position-relative contact">
    <div class="contact__container position-fixed d-flex align-items-center p-0">
        <div class="d-flex align-items-center ms-auto justify-content-center">
            <div class="d-inline-block py-2 d-none d-sm-none d-md-block">
                {!! config('images.images.logo') !!}
            </div>
            <p class="p-0 my-0 mx-3 text-light text-13 d-none d-sm-none d-md-block">
                Lorem ipsum dolor sit amet,<br>
                consectetur adipiscing elit, ES
            </p>
            <div class="d-flex align-items-center">
                <a class="mx-2" href="#">{!! config('images.icons.facebook') !!}</a>
                <a class="mx-2" href="#">{!! config('images.icons.instagram') !!}</a>
                <p class="p-0 my-0 mx-3 text-light text-13 ">
                    condominioverdeserrano</p>
            </div>
        </div>
        <div class="contact__whatsapp">
            <a href="#" class="d-flex align-items-center text-decoration-none ms-5 py-1">
                {!! config('images.icons.whatsapp') !!}
                <div class="mx-3 d-none d-sm-none d-md-block">
                    <p class="m-0  text-light text-13">ATENDIMENTO</p>
                    <p class="m-0 fw-bold text-light text-17">POR WHATSAPP</p>
                </div>
            </a>
        </div>
    </div>
</div>

