<div class="d-flex align-items-end">
    <div class="description-section w-100 d-flex justify-content-end align-items-center row p-0 m-0">
        <div class="col-12 col-md-6 m-0 p-0">
            <ul class="ps-5 description__list w-100 py-5 text-light list-unstyled">
                <li>- Arquitetura Ecológica de Vanguarda</li>
                <li>- Espaços Verdes Privativos</li>
                <li>- Tecnologia Ecoeficiente</li>
                <li class="pb-3">- Instalações de Lazer de Classe Mundial</li>
            </ul>
        </div>
    </div>
</div>

<style>
    .description-section {
        background-image: url("{{url('/').config('images.images.background-description')}}");
        height: 390px;
        background-size: auto 100vh;
        background-repeat: no-repeat;
        background-position: center;
        position: relative;
    }
</style>
