<div class="house-plan pb-5">
    <h2 class="text-center mb-2">4 opções de plantas, 1 ou 2 andares.</h2>
    <p class="text-center fs-5 ">Unidades de 53 a 85m²</p>
    <div class="environments__cards row justify-content-between mt-5 px-5 align-items-stretch mb-5">
        <img class="col-12 col-md-5 col-lg-4 mb-2" src="{{config('images.images.home-plan-1')}}" alt="{{config('images.images.home-plan-1')}}">
        <img class="col-12 col-md-5 col-lg-4 mb-2" src="{{config('images.images.home-plan-2')}}" alt="{{config('images.images.home-plan-2')}}">
        <img class="col-12 col-md-5 col-lg-4 mb-2" src="{{config('images.images.home-plan-3')}}" alt="{{config('images.images.home-plan-3')}}">
    </div>
    <a href="#form" class="text-decoration-none"><button href="#form" type="submit" class="btn btn-primary px-5 mx-auto text-center d-block text-uppercase text-wrap">Cadastre-se e conheça as outras opções de plantas
            {!! config('images.icons.arrow-right') !!}</button></a>
</div>
