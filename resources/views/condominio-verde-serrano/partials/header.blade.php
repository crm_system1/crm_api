<div class="header__container w-100 position-relative">
     <div class="header__h1 position position-absolute d-flex justify-content-center">
         <div class="d-inline-block header__text-container">
             <p class="header__p condominio-text">CONDOMÍNIO</p>
             <p class="verde-serrano-text header__p ms-3 header__font-kefa">VERDE SERRANO</p>
         </div>
     </div>
 </div>
